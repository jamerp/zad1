# -*- encoding: utf-8 -*-

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.bind(('178.33.182.1', 31017))
s.listen(1)

try:
    while True:
        conn, addr = s.accept()

        correct = True

        result = 0
        for _ in range(2):
            try:
                result += float(conn.recv(1024))
            except:
                conn.sendall('ERROR')
                correct = False
                break
            else:
                conn.sendall('DONE')

        conn.sendall(str(result) if correct else '0')
finally:
    conn.close()
