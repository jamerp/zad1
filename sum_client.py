# -*- encoding: utf-8 -*-

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect(('178.33.182.1', 31017))

try:
    for varName in ['a', 'b']:
        while True:
            try:
                x = input(varName + '=')
            except:
                print '!!! TYPE NUMBER !!!'
            else:
                s.sendall(str(x))
                if s.recv(1024) == 'DONE':
                    break
                else:
                    print '!!! SERVER ERROR !!!'
                    exit()

    print 'a+b=' + s.recv(1024)
finally:
    s.close()
